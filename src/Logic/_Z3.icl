implementation module Logic._Z3

/**
 * Copyright 2021-2022 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import StdEnv
import System._Pointer

import code from library "-lz3"
import code from library "z3_library"

:: Z3Config :== Pointer
:: Z3Context :== Pointer
:: Z3Solver :== Pointer
:: Z3Optimize :== Pointer
:: Z3Model` :== Pointer

:: Z3Params :== Pointer

:: Z3Symbol :== Pointer

:: Z3Constructor :== Pointer
:: Z3Sort :== Pointer

:: Z3Ast :== Pointer
:: Z3App :== Pointer
:: Z3FuncDecl :== Pointer

:: Z3AstVector :== Pointer

instance fromInt Z3LBool
where
	fromInt -1 = Z3LFalse
	fromInt 0 = Z3LUndef
	fromInt 1 = Z3LTrue

instance fromInt Z3AstKind
where
	fromInt 0 = Z3NumeralAst
	fromInt 1 = Z3AppAst
	fromInt 2 = Z3VarAst
	fromInt 3 = Z3QuantifierAst
	fromInt 4 = Z3SortAst
	fromInt 5 = Z3FuncDeclAst

noContext :: Z3Context
noContext = 0

noSolver :: Z3Solver
noSolver = 0

isNoSolver :: Z3Solver -> Bool
isNoSolver sol = sol == 0

noOptimize :: Z3Optimize
noOptimize = 0

isNoOptimize :: Z3Optimize -> Bool
isNoOptimize sol = sol == 0

Z3_global_param_reset_all :: Bool
Z3_global_param_reset_all = reset True
where
	reset :: !Bool -> Bool
	reset _ = code {
		ccall Z3_global_param_reset_all ":V:I"
	}

Z3_global_param_set :: !String !String -> Bool
Z3_global_param_set par val = set (packString par) (packString val) True
where
	set :: !String !String !Bool -> Bool
	set _ _ _ = code {
		ccall Z3_global_param_set "ss:V:I"
	}

Z3_mk_config :: Z3Config
Z3_mk_config = code {
	ccall Z3_mk_config ":p"
}

Z3_set_param_value :: !String !String !Z3Config -> Z3Config
Z3_set_param_value par val cfg = set cfg (packString par) (packString val) cfg
where
	set :: !Z3Config !String !String !Z3Config -> Z3Config
	set _ _ _ _ = code {
		ccall Z3_set_param_value "pss:V:p"
	}

Z3_del_config :: !Z3Config !Z3Context -> Z3Context
Z3_del_config cfg ctx = code {
	ccall Z3_del_config "p:V:p"
}

Z3_mk_context :: !Z3Config -> Z3Context
Z3_mk_context cfg = code {
	ccall Z3_mk_context "p:p"
}

Z3_del_context :: !Z3Context -> Bool
Z3_del_context ctx = del ctx True
where
	del :: !Z3Context !Bool -> Bool
	del _ _ = code {
		ccall Z3_del_context "p:V:I"
	}

Z3_mk_params :: !Z3Context -> Z3Params
Z3_mk_params ctx = mk ctx
where
	mk :: !Pointer -> Pointer
	mk _ = code {
		ccall Z3_mk_params "p:p"
	}

Z3_params_set_bool :: !Z3Symbol !Bool !Z3Params !Z3Context -> Z3Context
Z3_params_set_bool sym val params ctx = set ctx params sym val ctx
where
	set :: !Pointer !Pointer !Pointer !Bool !Pointer -> Pointer
	set _ _ _ _ _ = code {
		ccall Z3_params_set_bool "pppI:V:p"
	}

Z3_params_dec_ref :: !Z3Params !Z3Context -> Z3Context
Z3_params_dec_ref params ctx = dec ctx params ctx
where
	dec :: !Pointer !Pointer !Pointer -> Pointer
	dec _ _ _ = code {
		ccall Z3_params_dec_ref "pp:V:p"
	}

Z3_mk_solver :: !Z3Context -> Z3Solver
Z3_mk_solver _ = code {
	ccall Z3_mk_solver "p:p"
}

Z3_solver_set_params :: !Z3Params !Z3Solver !Z3Context -> Z3Context
Z3_solver_set_params params sol ctx = set ctx sol params ctx
where
	set :: !Pointer !Pointer !Pointer !Pointer -> Pointer
	set _ _ _ _ = code {
		ccall Z3_solver_set_params "ppp:V:p"
	}

Z3_solver_assert :: !Z3Ast !Z3Solver !Z3Context -> Z3Context
Z3_solver_assert b sol ctx = assert ctx sol b ctx
where
	assert :: !Pointer !Pointer !Pointer !Pointer -> Pointer
	assert _ _ _ _ = code {
		ccall Z3_solver_assert "ppp:V:p"
	}

Z3_solver_check :: !Z3Solver !Z3Context -> Z3LBool
Z3_solver_check sol ctx = fromInt (check ctx sol)
where
	check :: !Pointer !Pointer -> Int
	check _ _ = code {
		ccall Z3_solver_check "pp:I"
	}

Z3_solver_check_assumptions :: ![Z3Ast] !Z3Solver !Z3Context -> Z3LBool
Z3_solver_check_assumptions assumptions sol ctx = fromInt (check ctx sol (length assumptions) {a \\ a <- assumptions})
where
	check :: !Pointer !Pointer !Int !{#Pointer} -> Int
	check _ _ _ _ = code {
		ccall Z3_solver_check "ppIA:I"
	}

Z3_solver_get_reason_unknown :: !Z3Solver !Z3Context -> String
Z3_solver_get_reason_unknown sol ctx = derefString (get ctx sol)
where
	get :: !Pointer !Pointer -> Pointer
	get _ _ = code {
		ccall Z3_solver_get_reason_unknown "pp:p"
	}

Z3_solver_get_model :: !Z3Solver !Z3Context -> Z3Model`
Z3_solver_get_model sol ctx = get ctx sol
where
	get :: !Pointer !Pointer -> Pointer
	get _ _ = code {
		ccall Z3_solver_get_model "pp:p"
	}

Z3_solver_get_unsat_core :: !Z3Solver !Z3Context -> Z3AstVector
Z3_solver_get_unsat_core sol ctx = get ctx sol
where
	get :: !Pointer !Pointer -> Pointer
	get _ _ = code {
		ccall Z3_solver_get_unsat_core "pp:p"
	}

Z3_solver_to_string :: !Z3Solver !Z3Context -> String
Z3_solver_to_string sol ctx = derefString (conv ctx sol)
where
	conv :: !Pointer !Pointer -> Pointer
	conv _ _ = code {
		ccall Z3_solver_to_string "pp:p"
	}

Z3_solver_dec_ref :: !Z3Solver !Z3Context -> Z3Context
Z3_solver_dec_ref sol ctx = dec ctx sol ctx
where
	dec :: !Pointer !Pointer !Pointer -> Pointer
	dec _ _ _ = code {
		ccall Z3_solver_dec_ref "pp:V:p"
	}

Z3_mk_optimize :: !Z3Context -> Z3Optimize
Z3_mk_optimize _ = code {
	ccall Z3_mk_optimize "p:p"
}

Z3_optimize_set_params :: !Z3Params !Z3Optimize !Z3Context -> Z3Context
Z3_optimize_set_params params opt ctx = set ctx opt params ctx
where
	set :: !Pointer !Pointer !Pointer !Pointer -> Pointer
	set _ _ _ _ = code {
		ccall Z3_optimize_set_params "ppp:V:p"
	}

Z3_optimize_assert :: !Z3Ast !Z3Optimize !Z3Context -> Z3Context
Z3_optimize_assert b opt ctx = assert ctx opt b ctx
where
	assert :: !Pointer !Pointer !Pointer !Pointer -> Pointer
	assert _ _ _ _ = code {
		ccall Z3_optimize_assert "ppp:V:p"
	}

Z3_optimize_check :: ![Z3Ast] !Z3Optimize !Z3Context -> Z3LBool
Z3_optimize_check assumptions opt ctx = fromInt (check ctx opt (length assumptions) {a \\ a <- assumptions})
where
	check :: !Pointer !Pointer !Int !{#Pointer} -> Int
	check _ _ _ _ = code {
		ccall Z3_optimize_check "ppIA:I"
	}

Z3_optimize_get_reason_unknown :: !Z3Optimize !Z3Context -> String
Z3_optimize_get_reason_unknown opt ctx = derefString (get ctx opt)
where
	get :: !Pointer !Pointer -> Pointer
	get _ _ = code {
		ccall Z3_optimize_get_reason_unknown "pp:p"
	}

Z3_optimize_get_model :: !Z3Optimize !Z3Context -> Z3Model`
Z3_optimize_get_model opt ctx = get ctx opt
where
	get :: !Pointer !Pointer -> Pointer
	get _ _ = code {
		ccall Z3_optimize_get_model "pp:p"
	}

Z3_optimize_get_unsat_core :: !Z3Optimize !Z3Context -> Z3AstVector
Z3_optimize_get_unsat_core opt ctx = get ctx opt
where
	get :: !Pointer !Pointer -> Pointer
	get _ _ = code {
		ccall Z3_optimize_get_unsat_core "pp:p"
	}

Z3_optimize_maximize :: !Z3Ast !Z3Optimize !Z3Context -> Z3Context
Z3_optimize_maximize ast opt ctx = max ctx opt ast ctx
where
	max :: !Pointer !Pointer !Pointer !Pointer -> Pointer
	max _ _ _ _ = code {
		ccall Z3_optimize_maximize "ppp:V:p"
	}

Z3_optimize_minimize :: !Z3Ast !Z3Optimize !Z3Context -> Z3Context
Z3_optimize_minimize ast opt ctx = min ctx opt ast ctx
where
	min :: !Pointer !Pointer !Pointer !Pointer -> Pointer
	min _ _ _ _ = code {
		ccall Z3_optimize_minimize "ppp:V:p"
	}

Z3_optimize_to_string :: !Z3Optimize !Z3Context -> String
Z3_optimize_to_string opt ctx = derefString (conv ctx opt)
where
	conv :: !Pointer !Pointer -> Pointer
	conv _ _ = code {
		ccall Z3_optimize_to_string "pp:p"
	}

Z3_optimize_dec_ref :: !Z3Optimize !Z3Context -> Z3Context
Z3_optimize_dec_ref opt ctx = dec ctx opt ctx
where
	dec :: !Pointer !Pointer !Pointer -> Pointer
	dec _ _ _ = code {
		ccall Z3_optimize_dec_ref "pp:V:p"
	}

Z3_model_eval :: !Bool !Z3Ast !Z3Model` !Z3Context -> ?Z3Ast
Z3_model_eval complete arg mdl ctx
	# (ok,res) = eval ctx mdl arg complete
	= if (ok <> 0) (?Just res) ?None
where
	eval :: !Pointer !Pointer !Pointer !Bool -> (!Int, !Pointer)
	eval _ _ _ _ = code {
		ccall Z3_model_eval "pppI:Ip"
	}

Z3_model_get_const_decl :: !Int !Z3Model` !Z3Context -> Z3FuncDecl
Z3_model_get_const_decl i mdl ctx = get ctx mdl i
where
	get :: !Pointer !Pointer !Int -> Pointer
	get _ _ _ = code {
		ccall Z3_model_get_const_decl "ppI:p"
	}

Z3_model_get_const_interp :: !Z3FuncDecl !Z3Model` !Z3Context -> ?Z3Ast
Z3_model_get_const_interp const mdl ctx = case get ctx mdl const of
	0 -> ?None // the value of const does not matter in mdl
	p -> ?Just p
where
	get :: !Pointer !Pointer !Pointer -> Pointer
	get _ _ _ = code {
		ccall Z3_model_get_const_interp "ppp:p"
	}

Z3_model_get_num_consts :: !Z3Model` !Z3Context -> Int
Z3_model_get_num_consts mdl ctx = get ctx mdl
where
	get :: !Pointer !Pointer -> Int
	get _ _ = code {
		ccall Z3_model_get_num_consts "pp:I"
	}

Z3_model_to_string :: !Z3Model` !Z3Context -> String
Z3_model_to_string mdl ctx = derefString (conv ctx mdl)
where
	conv :: !Pointer !Pointer -> Pointer
	conv _ _ = code {
		ccall Z3_model_to_string "pp:p"
	}

Z3_mk_int_symbol :: !Int !Z3Context -> Z3Symbol
Z3_mk_int_symbol i ctx = mk ctx i
where
	mk :: !Pointer !Int -> Pointer
	mk _ _ = code {
		ccall Z3_mk_int_symbol "pI:p"
	}

Z3_mk_string_symbol :: !String !Z3Context -> Z3Symbol
Z3_mk_string_symbol s ctx = mk ctx (packString s)
where
	mk :: !Pointer !String -> Pointer
	mk _ _ = code {
		ccall Z3_mk_string_symbol "ps:p"
	}

Z3_get_symbol_string :: !Z3Symbol !Z3Context -> String
Z3_get_symbol_string sym ctx = derefString (get ctx sym)
where
	get :: !Pointer !Pointer -> Pointer
	get _ _ = code {
		ccall Z3_get_symbol_string "pp:p"
	}

Z3_get_ast_kind :: !Z3Ast !Z3Context -> ?Z3AstKind
Z3_get_ast_kind ast ctx
	# kind = get ctx ast
	| kind < 1000 = ?Just (fromInt kind)
	| otherwise = ?None
where
	get :: !Pointer !Pointer -> Int
	get _ _ = code {
		ccall Z3_get_ast_kind "pp:I"
	}

Z3_ast_to_string :: !Z3Ast !Z3Context -> String
Z3_ast_to_string ast ctx = derefString (conv ctx ast)
where
	conv :: !Pointer !Pointer -> Pointer
	conv _ _ = code {
		ccall Z3_ast_to_string "pp:p"
	}

Z3_to_app :: !Z3Ast !Z3Context -> Z3App
Z3_to_app ast ctx = conv ctx ast
where
	conv :: !Pointer !Pointer -> Pointer
	conv _ _ = code {
		ccall Z3_to_app "pp:p"
	}

Z3_get_app_arg :: !Int !Z3App !Z3Context -> Z3Ast
Z3_get_app_arg i app ctx = get ctx app i
where
	get :: !Pointer !Pointer !Int -> Pointer
	get _ _ _ = code {
		ccall Z3_get_app_arg "ppI:p"
	}

Z3_get_app_decl :: !Z3App !Z3Context -> Z3FuncDecl
Z3_get_app_decl app ctx = get ctx app
where
	get :: !Pointer !Pointer -> Pointer
	get _ _ = code {
		ccall Z3_get_app_decl "pp:p"
	}

Z3_get_app_num_args :: !Z3App !Z3Context -> Int
Z3_get_app_num_args app ctx = get ctx app
where
	get :: !Pointer !Pointer -> Int
	get _ _ = code {
		ccall Z3_get_app_num_args "pp:I"
	}

Z3_mk_func_decl :: !Z3Symbol ![Z3Sort] !Z3Sort !Z3Context -> Z3FuncDecl
Z3_mk_func_decl sym argSorts retSort ctx = mk ctx sym (length argSorts) {s \\ s <- argSorts} retSort
where
	mk :: !Pointer !Pointer !Int !{#Pointer} !Pointer -> Pointer
	mk _ _ _ _ _ = code {
		ccall Z3_mk_func_decl "ppIAp:p"
	}

Z3_func_decl_to_ast :: !Z3FuncDecl !Z3Context -> Z3Ast
Z3_func_decl_to_ast decl ctx = conv ctx decl
where
	conv :: !Pointer !Pointer -> Pointer
	conv _ _ = code {
		ccall Z3_func_decl_to_ast "pp:p"
	}

Z3_to_func_decl :: !Z3Ast !Z3Context -> Z3FuncDecl
Z3_to_func_decl ast ctx = conv ctx ast
where
	conv :: !Pointer !Pointer -> Pointer
	conv _ _ = code {
		ccall Z3_to_func_decl "pp:p"
	}

Z3_get_decl_name :: !Z3FuncDecl !Z3Context -> Z3Symbol
Z3_get_decl_name decl ctx = get ctx decl
where
	get :: !Pointer !Pointer -> Pointer
	get _ _ = code {
		ccall Z3_get_decl_name "pp:p"
	}

Z3_get_func_decl_id :: !Z3FuncDecl !Z3Context -> Int
Z3_get_func_decl_id decl ctx = get ctx decl
where
	get :: !Pointer !Pointer -> Int
	get _ _ = code {
		ccall Z3_get_func_decl_id "pp:I"
	}

Z3_mk_constant_constructor :: !Z3Symbol !Z3Symbol !Z3Context -> Z3Constructor
Z3_mk_constant_constructor name recognizer ctx = mk ctx name recognizer 0 0 0 0
where
	mk :: !Pointer !Pointer !Pointer !Pointer !Pointer !Pointer !Pointer -> Pointer
	mk _ _ _ _ _ _ _ = code {
		ccall Z3_mk_constructor "ppppppp:p"
	}

Z3_query_constant_constructor :: !Z3Constructor !Z3Context -> (!Z3FuncDecl, !Z3FuncDecl)
Z3_query_constant_constructor cons ctx
	#! (cons,recognizer,_) = query ctx cons 0
	= (cons,recognizer)
where
	query :: !Pointer !Pointer !Int -> (!Pointer, !Pointer, !Pointer)
	query _ _ _ = code {
		ccall Z3_query_constructor "ppI:Vppp"
	}

Z3_del_constructor :: !Z3Constructor !Z3Context -> Z3Context
Z3_del_constructor cons ctx = del ctx cons ctx
where
	del :: !Pointer !Pointer !Pointer -> Pointer
	del _ _ _ = code {
		ccall Z3_del_constructor "pp:V:p"
	}

Z3_mk_datatype :: !Z3Symbol ![Z3Constructor] !Z3Context -> Z3Sort
Z3_mk_datatype name constructors ctx = mk ctx name (length constructors) {c \\ c <- constructors}
where
	mk :: !Pointer !Pointer !Int !{#Pointer} -> Pointer
	mk _ _ _ _ = code {
		ccall Z3_mk_datatype "ppIA:p"
	}

Z3_mk_int_sort :: !Z3Context -> Z3Sort
Z3_mk_int_sort _ = code {
	ccall Z3_mk_int_sort "p:p"
}

Z3_is_eq_ast :: !Z3Ast !Z3Ast !Z3Context -> Bool
Z3_is_eq_ast x y ctx = eq ctx x y
where
	eq :: !Pointer !Pointer !Pointer -> Bool
	eq _ _ _ = code {
		ccall Z3_is_eq_ast "ppp:I"
		| The Z3 bool is a char, so we need to do a bitwise AND
		pushI 255
		and%
	}

Z3_mk_and :: ![Z3Ast] !Z3Context -> Z3Ast
Z3_mk_and terms ctx = mk ctx (length terms) {t \\ t <- terms}
where
	mk :: !Pointer !Int !{#Pointer} -> Pointer
	mk _ _ _ = code {
		ccall Z3_mk_and "pIA:p"
	}

Z3_mk_app :: !Z3FuncDecl ![Z3Ast] !Z3Context -> Z3Ast
Z3_mk_app fun args ctx = mk ctx fun (length args) {a \\ a <- args}
where
	mk :: !Pointer !Pointer !Int !{#Pointer} -> Pointer
	mk _ _ _ _ = code {
		ccall Z3_mk_app "ppIA:p"
	}

Z3_mk_eq :: !Z3Ast !Z3Ast !Z3Context -> Z3Ast
Z3_mk_eq x y ctx = mk ctx x y
where
	mk :: !Pointer !Pointer !Pointer -> Pointer
	mk _ _ _ = code {
		ccall Z3_mk_eq "ppp:p"
	}

Z3_mk_false :: !Z3Context -> Z3Ast
Z3_mk_false ctx = code {
	ccall Z3_mk_false "p:p"
}

Z3_mk_int :: !Int !Z3Sort !Z3Context -> Z3Ast
Z3_mk_int i type ctx = mk ctx i type
where
	mk :: !Pointer !Int !Pointer -> Pointer
	mk _ _ _ = code {
		ccall Z3_mk_int "pIp:p"
	}

Z3_mk_ite :: !Z3Ast !Z3Ast !Z3Ast !Z3Context -> Z3Ast
Z3_mk_ite x y z ctx = mk ctx x y z
where
	mk :: !Pointer !Pointer !Pointer !Pointer -> Pointer
	mk _ _ _ _ = code {
		ccall Z3_mk_ite "pppp:p"
	}

Z3_mk_or :: ![Z3Ast] !Z3Context -> Z3Ast
Z3_mk_or terms ctx = mk ctx (length terms) {t \\ t <- terms}
where
	mk :: !Pointer !Int !{#Pointer} -> Pointer
	mk _ _ _ = code {
		ccall Z3_mk_or "pIA:p"
	}

Z3_mk_true :: !Z3Context -> Z3Ast
Z3_mk_true ctx = code {
	ccall Z3_mk_true "p:p"
}

Z3_ast_vector_get :: !Z3AstVector !Int !Z3Context -> Z3Ast
Z3_ast_vector_get vec i ctx = get ctx vec i
where
	get :: !Pointer !Pointer !Pointer -> Pointer
	get _ _ _ = code {
		ccall Z3_ast_vector_get "ppp:p"
	}

Z3_ast_vector_size :: !Z3AstVector !Z3Context -> Int
Z3_ast_vector_size vec ctx = size ctx vec
where
	size :: !Pointer !Pointer -> Int
	size _ _ = code {
		ccall Z3_ast_vector_size "pp:I"
	}

Z3_ast_vector_dec_ref :: !Z3AstVector !Z3Context -> Z3Context
Z3_ast_vector_dec_ref vec ctx = dec ctx vec ctx
where
	dec :: !Pointer !Pointer !Pointer -> Pointer
	dec _ _ _ = code {
		ccall Z3_ast_vector_dec_ref "pp:V:p"
	}
