definition module Logic.Z3

/**
 * A simple high-level interface to the Z3 theorem prover.
 *
 * Copyright 2021-2022 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

from Control.Applicative import class <*>, class pure, class Applicative
from Control.Monad import class Monad
from Control.Monad.Fail import class MonadFail
from Data.Error import :: MaybeError
from Data.Functor import class Functor
from Data.Map import :: Map

from Logic._Z3 import :: Z3FuncDecl, :: Z3LBool(..), :: Z3Sort

:: Z3State
:: Z3Result a
:: Z3 a (=: Z3 (Z3State -> MaybeError String (Z3Result a)))

instance Functor Z3
instance pure Z3
instance <*> Z3
instance Monad Z3
instance MonadFail Z3

/**
 * Expressions in the Z3 AST.
 * NB: this type is not complete.
 */
:: Z3Expr
	= Z3True //* True constant.
	| Z3False //* False constant.
	| Z3Int !Int //* Integer constant.
	| Z3Eq !Z3Expr !Z3Expr //* Equality.
	| Z3Ite !Z3Expr !Z3Expr !Z3Expr //* If-then-else.
	| Z3And ![Z3Expr] //* Logical conjunction.
	| Z3Or ![Z3Expr] //* Logical disjunction.
	| Z3App !Z3FuncDecl ![Z3Expr] //* A function applied to a number of arguments.

/**
 * After checking satisfiability, a model is a mapping from the names of
 * constants (defined with `makeConst`) to their values.
 *
 * Not every created constant may be present. When a constant is not present,
 * this means that its value does not matter in the model.
 */
:: Z3Model :== Map String Z3Expr

//* Evaluate a `Z3` monad.
evalZ3 :: !(Z3 a) -> MaybeError String a

/**
 * Creates an enumeration type.
 * @param The name of the type.
 * @param The names for the constructors. Each constructor will be defined, and
 *   for each constructor a recognizer function `is<Cons>` will be defined.
 * @result The newly created sort.
 * @result For each constructor, (1) the constructor itself and (2) a function
 *   that checks if a value is of that constructor.
 */
makeEnum :: !String ![String] -> Z3 (Z3Sort, [(Z3FuncDecl, Z3FuncDecl)])

//* Create a new constant.
makeConst :: !String !Z3Sort -> Z3 Z3FuncDecl

//* Check if two functions are equal.
eqFuncDecl :: !Z3FuncDecl !Z3FuncDecl -> Z3 Bool

/**
 * Run a `Z3` monad with a solver in the context. This is required to use some
 * functions, like `assert` and `check`.
 */
withSolver :: !(Z3 a) -> Z3 a

/**
 * Assert that an expression holds.
 * This function can only be used within `withSolver` or `withOptimizer`.
 */
assert :: !Z3Expr -> Z3 ()

/**
 * Check the assertions in the solver or optimizer.
 * After this, `getModel` can be used to retrieve the model.
 * This function can only be used within `withSolver` or `withOptimizer`.
 */
check :: Z3 Z3LBool

/**
 * Check that the assertions in the solver or optimizer are compatible with an
 * additional list of assumptions.
 *
 * If this succeeds, `getModel` can be used to retrieve the model. If the
 * result is `Z3LFalse`, the list of expressions is not empty and contains the
 * indexes of the incompatible assumptions.
 *
 * This function can only be used within `withSolver` or `withOptimizer`.
 *
 * When using `withOptimizer`, the model built (if any) may not be optimized
 * correctly. You can run `checkWithAssumptions` first, and if it succeeds,
 * `assert` the assumptions and run `check` to get the optimized model.
 */
checkWithAssumptions :: ![Z3Expr] -> Z3 (Z3LBool, [Int])

//* Get the reason why a `check` or `checkWithAssumptions` returned `Z3LUndef`.
getReasonForUndef :: Z3 String

/**
 * Run a `Z3` monad with an optimizer in the context. This allows the use of
 * `assert` and `check` (like `withSolver`), but additionally allows `maximize`
 * and `minimize`.
 */
withOptimizer :: !(Z3 a) -> Z3 a

/**
 * Add a maximization goal.
 * This function can only be used within `withOptimizer.
 */
maximize :: !Z3Expr -> Z3 ()

/**
 * Add a minimization goal.
 * This function can only be used within `withOptimizer.
 */
minimize :: !Z3Expr -> Z3 ()

/**
 * Retrieve the model.
 * This is only guaranteed to work after `check` returned `Z3LTrue`.
 */
getModel :: Z3 Z3Model
