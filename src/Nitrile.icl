module Nitrile

/**
 * Copyright 2021-2023 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import Data.Error
import StdEnv
import StdMaybe
import System.CommandLine
import System.FilePath
import System.OS

import Nitrile.CLI
import Nitrile.CLI.Settings
import Nitrile.CLI.Util
import Nitrile.CLI.Util.Legal

Start w
	# (mbSettings,w) = getSettings w
	# (settings,w) = case mbSettings of
		Ok settings -> (settings, w)
		Error e -> (defaultGlobalSettings, log e w)
	# (cmd=:[prog:_],w) = getCommandLine w
	= case parseCommandLine settings cmd of
		Error e
			-> abort (e +++ "\n")
		Ok (cmd, opts)
			// The legal header is not printed for run to avoid interfering with stdout communication.
			# w = if
				(cmd=:Env || cmd=:(Run _) || cmd=:Terms || opts.no_header)
				w
				(printLegalHeader ?None (dropDirectory prog) w)
			# w = if (IF_WINDOWS (isJust opts.GlobalOptions.parallel_jobs) False) (log "NB: --parallel-jobs has no effect on Windows." w) w
			# (ok,w) = performCommand cmd opts w
			| not ok
				-> setReturnCode 1 w
				-> w
