implementation module System.Watchman

/**
 * Copyright 2022 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import Control.Applicative
import Data.Error
import Data.Func
import Data.Functor
import Data.List
import Data.Maybe
import Data.Tuple
import StdDebug
import StdEnv
import System.FilePath
import System.OS
import System.Process
import Text
import Text.GenJSON

JSONEncode{|Expression|} _ e = case e of
	AllOf es ->
		[JSONArray [JSONString "allof":map toJSON es]]
	AnyOf es ->
		[JSONArray [JSONString "anyof":map toJSON es]]
	Dirname dir ?None ->
		[JSONArray [JSONString "dirname", JSONString dir]]
	Dirname dir (?Just depth) ->
		[JSONArray [JSONString "dirname", JSONString dir, JSONArray [JSONString "depth":JSONEncode{|*|} False depth]]]
	Not e ->
		[JSONArray [JSONString "not", toJSON e]]
	Suffix s ->
		[JSONArray [JSONString "suffix", JSONString s]]
	FileType ft ->
		[JSONArray [JSONString "type", JSONString (toString ft)]]
	Wholename names ->
		[JSONArray [JSONString "name", JSONArray (map JSONString names), JSONString "wholename"]]
	WholenameRegex pcre ->
		[JSONArray [JSONString "pcre", JSONString pcre, JSONString "wholename"]]

JSONEncode{|IntConstraint|} _ cstr = case cstr of
	Eq i -> [JSONString "eq", JSONInt i]
	Ne i -> [JSONString "ne", JSONInt i]
	Gt i -> [JSONString "gt", JSONInt i]
	Ge i -> [JSONString "ge", JSONInt i]
	Lt i -> [JSONString "lt", JSONInt i]
	Le i -> [JSONString "le", JSONInt i]

instance toString FileType
where
	toString Directory = "d"
	toString RegularFile = "f"

derive JSONDecode SubscribeResponse, ChangeNotification

JSONDecode{|Response|} _ json=:[JSONObject fields:rest]
	| isEmpty trimmed_fields = /* e.g. if only an error field is given */
		( ?Just (toResponse ?None)
		, rest
		)
	| otherwise =
		(
			decode SubscribeResponse <|>
			decode ChangeNotification
		, rest
		)
where
	trimmed_fields = [f \\ f=:(k,_) <- fields | k <> "version" && k <> "error" && k <> "warning"]

	decode f = toResponse <$> ?Just <$> f <$> fst (JSONDecode{|*|} False [JSONObject trimmed_fields:rest])

	toResponse r =
		{ errors   = findString "error"
		, warnings = findString "warning"
		, response = r
		}
	where
		findString f = case lookup f fields of
			?Just (JSONString s) -> [s]
			?Just _ -> ["unknown " +++ f]
			?None -> []
JSONDecode{|Response|} _ json = (?None, json)

:: Command
	= Subscribe !FilePath !String !SubscribeOptions

:: SubscribeOptions =
	{ expression :: !Expression
	, fields     :: ![String]
	}

derive JSONEncode SubscribeOptions

JSONEncode{|Command|} _ cmd = case cmd of
	Subscribe path name opts ->
		[JSONArray [JSONString "subscribe", JSONString path, JSONString name, toJSON opts]]

watchDirectory :: !FilePath !Expression !*World -> (!MaybeError String Watchman, !*World)
watchDirectory root expr w
	// We need to add the watch before we can subscribe to it. For whatever
	// reason, `watchman watch PATH` fails in callProcess but works in
	// callProcessAndCollectOutput (on Windows).
	# (mbExitCode,w) = appFst (fmap fst3) (callProcessAndCollectOutput "watchman" ["watch", root] ?None w)
	| isError mbExitCode = (Error "Failed to start watchman (is it installed?)", w)
	| fromOk mbExitCode <> 0 = (Error "Failed to register current directory with watchman.", w)
	# (mbHandleAndIO,w) = runProcessIO "watchman" ["-j", "-p", "--no-pretty"] ?None w
	| isError mbHandleAndIO = (Error "Failed to start watchman (is it installed?)", w)
	# (handle,io) = fromOk mbHandleAndIO
	# (mbError,w) = writePipe (toString (toJSON (Subscribe root "" {expression=expr, fields=["name"]}))) io.stdIn w
	| isError mbError = (Error "Failed to subscribe to watchman.", w)
	# watchman =
		{ processHandle = handle
		, processIO = io
		, buffer = ""
		, responses = []
		}
	= (Ok watchman, w)

appendOutput :: !String !Watchman -> MaybeError String Watchman
appendOutput out wm
	# wm & buffer = wm.buffer +++ out
	  lines = split "\n" wm.buffer
	| length lines == 1
		= Ok wm
	# wm & buffer = last lines
	  new_responses = map (fromJSON o fromString) (init lines)
	| any isNone new_responses
		= Error ("Failed to parse watchman response: " +++ hd [line \\ ?None <- new_responses & line <- lines])
	# wm & responses = wm.responses ++ catMaybes new_responses
	= Ok wm
