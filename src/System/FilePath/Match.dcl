definition module System.FilePath.Match

/**
 * Copyright 2021-2022 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 *
 * @property-bootstrap
 *   import StdEnv
 *   from Data.Foldable import class Foldable(foldr1()), instance Foldable []
 *
 *   testMatches pattern yes no =
 *     foldr1 (/\)
 *       ([name (pattern +++ " accepts " +++ y) (matches pattern y) \\ y <- yes] ++
 *         [name (pattern +++ " rejects " +++ n) (not (matches pattern n)) \\ n <- no])
 */

from System.FilePath import :: FilePath

matchesAny :: ![String] !FilePath -> Bool

/**
 * Tests if the path is matched by the pattern. The pattern follows the
 * .gitignore rules (https://git-scm.com/docs/gitignore#_pattern_format), with
 * a few adaptations:
 *
 * - `/` as a directory separator on all platforms
 * - Patterns with a `/` at a non-final position are interpreted relative to
 *   the root
 * - Patterns ending on `/` are only matched by directories
 * - `*` matches any number of characters except `/`
 * - `?` matches any one character except `?`
 * - Leading `**``/` matches the following pattern in all directories
 * - Trailing `/``**` matches anything in a directory (but not the directory
 *   itself)
 * - `/``**`/` matches any number of directories
 * - `**` in any other environment functions as `*`
 *
 * Adaptations:
 *
 * - Comment lines (starting with `#`) are not supported
 * - Negation (leading `!`) is not supported
 * - Trailing spaces should not be escaped
 * - Range notation (`[a-zA-Z]`) is not implemented (yet)
 *
 * @param The pattern.
 * @param The file path. Directory paths should end in `/`.
 * @result Whether the file path is matched by the pattern.
 *
 * @property patterns without slashes:
 *   testMatches "foo"
 *     ["foo", "/foo", "a/foo", "a/b/foo", "a/foo/b"]
 *     ["afoo", "fooa"]
 * @property patterns with trailing slashes:
 *   testMatches "foo/"
 *     ["foo/", "/foo/", "a/foo/", "a/b/foo/", "a/foo/b"]
 *     ["foo", "x/foo"]
 * @property patterns with slashes at the beginning and/or middle:
 *   testMatches "/foo"
 *     ["foo", "/foo", "/foo/bar"]
 *     ["foox", "a/foo"] /\
 *   testMatches "x/y"
 *     ["x/y", "/x/y", "/x/y/", "x/y/z"]
 *     ["a/x/y"]
 * @property question mark:
 *   testMatches "a?b"
 *     ["axb"]
 *     ["a/b", "ab", "axyb"]
 * @property asterisk:
 *   testMatches "a*b"
 *     ["ab", "axb", "axyb"]
 *     ["a/b", "ax/b", "ax/xb", "a/x/b"]
 * @property leading double asterisk:
 *   testMatches ("**"+++"/foo")
 *     ["foo", "/foo", "a/foo", "a/b/foo"]
 *     ["xfoo", "a/xfoo"] /\
 *   testMatches ("**"+++"/foo/bar")
 *     ["foo/bar", "/foo/bar", "a/foo/bar", "a/b/foo/bar"]
 *     ["foo/xbar", "xfoo/bar", "foo/a/bar"]
 * @property trailing double asterisk:
 *   testMatches ("foo/"+++"**")
 *     ["foo/x", "/foo/x/y"]
 *     ["foo", "bar/x"]
 * @property double asterisk between slashes:
 *   testMatches ("a/"+++"**"+++"/b")
 *     ["a/b", "a/x/b", "a/x/y/b"]
 *     ["ab", "axb"]
 * @property regular double asterisk:
 *   testMatches "a**b"
 *     ["ab", "axb", "axyb"]
 *     ["a/b", "ax/b", "ax/xb", "a/x/b"]
 */
matches :: !String !FilePath -> Bool
