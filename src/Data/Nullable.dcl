definition module Data.Nullable

/**
 * Copyright 2021-2022 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

from Data.Error import :: MaybeError
from Data.Functor import class Functor
from Text.YAML import :: YAMLErrorWithLocations, :: YAMLNode, :: YAMLSchema, generic gConstructFromYAML

/**
 * This type is meant to be used in the type variable of a `?` type in a record
 * field for which `gConstructFromYAML` is derived, for instance:
 *
 * ```clean
 * :: R = {path :: !?(Nullable FilePath)}
 * ```
 *
 * This then has the following behaviour:
 *
 * - When the field is not specified, the value is `?None`
 * - When the field is specified as `null`, the value is `?Just Null`
 * - When the field is specified as some other value mapping to `FilePath` (in
 *   this example), the value is `?Just (NonNull v)`
 *
 * This makes it possible to distinguish between unspecified fields and fields
 * specified as `null` *past* the construction phase. This is for example
 * useful if you have a setup where one record value can override fields from
 * another value: you then need to be able to explicitly set values to `null`
 * in the overriding value.
 */
:: Nullable a = Null | NonNull !a

instance Functor Nullable where fmap :: !(a -> b) !(Nullable a) -> Nullable b

toMaybe :: !(Nullable a) -> ?a

derive gConstructFromYAML Nullable
