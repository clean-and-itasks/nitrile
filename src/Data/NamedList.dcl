definition module Data.NamedList

/**
 * Copyright 2021-2022 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

from Data.Error import :: MaybeError
from Text.GenJSON import :: JSONNode, generic JSONEncode
from Text.YAML import :: YAMLErrorWithLocations
from Text.YAML.Compose import :: YAMLNode
from Text.YAML.Construct import generic gConstructFromYAML
from Text.YAML.Schemas import :: YAMLSchema

:: NamedList e =: NamedList [(String, e)]

fromNamedList :: !(NamedList e) -> [(String, e)]

derive JSONEncode NamedList
derive gConstructFromYAML NamedList

/**
 * Filter the named list so that it only contains the given names.
 *
 * @param Names to select.
 * @param Named list to filter.
 * @return Filtered list when all names are found, list of missing names otherwise.
 */
onlyNames :: ![String] !(NamedList e) -> MaybeError [String] (NamedList e)
