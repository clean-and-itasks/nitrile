implementation module Nitrile.Metadata

/**
 * Copyright 2021-2022 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import Data.Maybe
import Data.Tuple
import SPDX.License
import StdEnv
import Text.GenJSON

import Data.NamedList
import Data.SemVer.Constraint
import Nitrile.Package
import Nitrile.Target

derive JSONEncode Metadata

JSONEncode{|MetadataDependency|} in_record rec=:{MetadataDependency|version,optional}
	| fromMaybe False optional =
		JSONEncode in_record rec
	| otherwise =
		[toJSON version]
where
	derive JSONEncode MetadataDependency

toMetadata :: !(?String) !Package -> Metadata
toMetadata git_ref pkg =
	{ maintainer = pkg.Package.maintainer
	, contact_email = pkg.Package.contact_email
	, description = pkg.Package.description
	, license = pkg.Package.license
	, git_ref = git_ref
	, dependencies = NamedList use_dependencies
	, extra_dependencies = NamedList
		[ (toString target, NamedList extra_dependencies)
		\\ p <- [AnyPlatform, Linux, Mac, Windows]
		, f <- [AnyArchitectureFamily, Intel, ARM]
		, bw <- [AnyBitwidth, BW32, BW64]
		// NB: we cannot use allTargets here, because we have to include AnyPlatform etc.
		, let
			target = {platform=p, architecture={family=f, bitwidth=bw}}
			new_dependencies = dependenciesInScope UseScope (normalizePackage target pkg)
			extra_dependencies =
				[ (pkg, {MetadataDependency|version=version, optional=if opt (?Just True) ?None})
				\\ (pkg,(opt,version)) <- fromNamedList new_dependencies
				| isEmpty (filter (((==) pkg) o fst) use_dependencies)
				]
		| isValidTarget target && not (isEmpty extra_dependencies)
		]
	}
where
	use_dependencies =
		[ (pkg, {MetadataDependency|version=version, optional=if opt (?Just True) ?None})
		\\ (pkg,(opt,version)) <- fromNamedList (dependenciesInScope UseScope pkg)
		]
