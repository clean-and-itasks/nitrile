implementation module Nitrile.Constants

/**
 * Copyright 2021-2022 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import Data.Error
import StdEnv
import System.Directory
import System.Environment
import System.FilePath
import System.OS
import Text

import Data.SemVer
import Nitrile.Target

currentVersion :: Version
currentVersion =: fromOk (parseVersion CURRENT_VERSION)

globalNitrileDir :: !*World -> (!MaybeError String FilePath, !*World)
globalNitrileDir w
	# (mbHome,w) = getEnvironmentVariable var w
	= case mbHome of
		?None -> (Error (var +++ " is not set"), w)
		?Just home -> (Ok (home </> IF_WINDOWS "Nitrile" ".nitrile"), w)
where
	var = IF_WINDOWS "APPDATA" "HOME"

constraintSetFile :: !Target -> FilePath
constraintSetFile target = concat3 "constraint-set-" (toString target) ".json"

localPackagesDir :: !Target -> FilePath
localPackagesDir target = LOCAL_PACKAGES_DIR </> toString target

testsDir :: !Target -> FilePath
testsDir target = TESTS_DIR </> toString target

homeDir :: !Target -> FilePath
homeDir target = localPackagesDir target </> ".home"

tmpDir :: !*World -> (!FilePath, !*World)
tmpDir w
	# (mbDir,w) = getCurrentDirectory w
	= case mbDir of
		Ok dir -> (dir </> "nitrile-tmp", w)
		Error e -> abort "Failed to get current working directory\n"
