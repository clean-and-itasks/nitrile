definition module Nitrile.Build

/**
 * Copyright 2021-2023 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

from Data.Error import :: MaybeError
from System.FilePath import :: FilePath

from Data.NamedList import :: NamedList
from Data.Nullable import :: Nullable
from Data.SemVer import :: Version
from Nitrile.Target import :: Target

:: Build =
	{ depends               :: !?[String]
	, script                :: !?[BuildCommand]
	, required_dependencies :: !?[String]
	}

:: BuildCommand
	= Clm !(ClmBuildOptions String)
	| TestRunner !TestRunnerOptions
	| SystemCommand !String

:: ClmBuildOptions main =
	{ main                  :: !main
	, target                :: !?String
	, src                   :: !?[FilePath]
	, compiler              :: !?String
	, compiler_options      :: !?[String]
	// Compilation options:
	, undecidable_instances :: !?Bool
	, check_indices         :: !?Bool
	, partial_functions     :: !?PartialFunctionHandling
	// Project options:
	, profiling             :: !?Profiling
	, generate_descriptors  :: !?Bool
	, export_local_labels   :: !?Bool
	, bytecode              :: !?(Nullable ByteCodeOptions)
	, list_types            :: !?ListTypes
	// Main module options (lifted to project options):
	, warnings              :: !?Bool
	, fusion                :: !?Fusion
	// Linker options:
	, strip                 :: !?Bool
	, link                  :: !?[FilePath]
	, post_link             :: !?(Nullable FilePath)
	// Application options:
	, heap                  :: !?String
	, stack                 :: !?String
	, print_constructors    :: !?Bool
	, print_result          :: !?Bool
	, print_time            :: !?Bool
	, wait_for_key_press    :: !?Bool
	, garbage_collector     :: !?GarbageCollector
	}

:: PartialFunctionHandling = IgnorePartialFunctions | WarningPartialFunctions | ErrorPartialFunctions

:: Profiling = NoProfiling | StackTracing | TimeProfiling | CallgraphProfiling

:: Fusion = NoFusion | PlainFusion | GenericFusion | AllFusion

:: GarbageCollector = CopyingCollector | MarkingCollector

:: ByteCodeOptions =
	{ optimize_abc :: !?Bool
	, prelink      :: !?Bool
	}

:: ListTypes = ListTypesNone | ListTypesInferred | ListTypesLackingStrictness | ListTypesAll

:: TestRunnerOptions =
	{ output_format :: !?TestRunnerOutputFormat
	, junit_xml     :: !?FilePath
	, tests         :: ![TestRunnerTest]
	}

:: TestRunnerOutputFormat = JSON | Human

:: TestRunnerTest =
	{ executable :: !FilePath
	, options    :: !?[String]
	}

defaultClmBuildOptions :: !main -> ClmBuildOptions main

/**
 * @param The 'default' options.
 * @param The 'overriding' options.
 * @result Options in which the 'overriding' option is preferred, unless it's
 *   `?None`, in which case the 'default' option is used.
 */
combineClmBuildOptions :: !(ClmBuildOptions a) !(ClmBuildOptions b) -> ClmBuildOptions b

/**
 * @param The clm version.
 * @param The linker to be used by `clm`.
 * @param The maximum number of parallel jobs (if `?None`, defaults to the
 *   number of processors available). `clm` does not implement this on Windows.
 * @param Directories to include.
 * @param The clm options.
 * @result Configuration options.
 * @result Path options.
 * @result Code generation options.
 * @result Application options.
 */
toClmOptions :: !Version !(?String) !(?Int) ![FilePath] !(ClmBuildOptions m) -> MaybeError String ([String], [String], [String], [String])
