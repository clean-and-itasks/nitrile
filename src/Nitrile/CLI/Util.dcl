definition module Nitrile.CLI.Util

/**
 * Copyright 2021-2023 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

from Data.Either import :: Either
from Data.Error import :: MaybeError
from StdOverloaded import class toString
from System.FilePath import :: FilePath
from Text.YAML import :: YAMLError, :: YAMLErrorWithLocations

from Data.SemVer import :: Version
from Data.SemVer.ConstraintSet import :: ConstraintSet
from Nitrile.Package import :: Package
from Nitrile.Registry import :: Registry
from Nitrile.Target import :: Target

:: ReadPackageError
	= MissingNitrileYml !String
	| OtherPackageError !String

instance toString ReadPackageError

log :: !String !*World -> *World
succeed :: !(?String) !*World -> (!Bool, !*World)
fail :: !String !*World -> (!Bool, !*World)

checkSpecificTarget :: !Target -> MaybeError String ()
logWarningForUnspecificTarget :: !String !Target !*World -> *World

packageSlur :: !Package !Target -> ?String
packageFile :: !Package !Target -> ?String
isPackageFile :: !String -> Bool

environmentVariables :: !Target !*World -> (![(String, String)], !*World)
setupEnvironmentVariables :: !Target !*World -> *World

/**
 * Gets the reference to a git commit in the current directory. A tag is
 * preferred, if it can be parsed as a semantic version (optionally prefixed by
 * `v`) and this version matches the first argument. Otherwise, a hash is used.
 * The function may return `?None` if `git` is not installed or we are not in a
 * git repository.
 */
getGitRefMatching :: !(?Version) !*World -> (!?String, !*World)

/**
 * Retrieves the registry packages from the local registry copy. Returns an
 * error if parsing/reading registry copy fails.
 */
getRegistry :: !*World -> (!MaybeError String Registry, !*World)

getConstraintSet :: !Target !*World -> (!MaybeError String ConstraintSet, !*World)

/**
 * Parses the nitrile.yml or nitrile.ncl file in a project directory.
 * If a nitrile.ncl file is present, it precedes a coexisting nitrile.yml file.
 *
 * @param The package specified by the --name CLI option, if any.
 * @param The predefined variables for use in nitrile.ncl files, a list of `(variableName, value)`.
 * @param The directory nitrile.yml or nitrile.ncl is expected to be located in.
 */
readPackage :: !(?String) ![(String, String)] !FilePath !*World -> (!MaybeError ReadPackageError Package, !*World)

printYAMLError :: !(Either YAMLError YAMLErrorWithLocations) -> String

/**
 * Calls `curl` with given arguments passing `stdout`/`stderr` to this
 * process's channels.
 *
 * @param The arguments passed to `curl`.
 * @param The directory to run `curl` in.
 * @result The error message in case there is any error running `curl` or with
 *   the request made.
 */
callCurlAndPassIO :: ![String] !(?String) !*World -> (!MaybeError String (), !*World)

/**
 * Calls `curl` with given arguments providing the response as result.
 *
 * @param The arguments passed to `curl`.
 * @param The directory to run `curl` in.
 * @result The result (`stdout` output of `curl`). The error message in case
 *   there is any error running `curl` or with the request made (`stderr`
 *   output of `curl` is included in the message in such cases).
 */
callCurlAndCollectOutput :: ![String] !(?String) !*World -> (!MaybeError String String, !*World)
