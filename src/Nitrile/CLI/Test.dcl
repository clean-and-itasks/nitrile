definition module Nitrile.CLI.Test

/**
 * Copyright 2021-2023 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

from Data.Map import :: Map
from System.FilePath import :: FilePath

from Data.SemVer import :: Version
from Nitrile.CLI import :: GlobalOptions
from Nitrile.CLI.Util.DependencySources import :: DependencySource, :: DependencySources
from Nitrile.Package import :: Package
from Nitrile.Target import :: Target

:: CLITestOptions =
	{ list                    :: !Bool //* Only list test names, do not run.
	, only                    :: !?[String] //* Test only the specified tests.
	, except                  :: !?[String] //* Test everything except the specified tests.
	, property_tester_options :: !CLIPropertyTesterOptions //* Options for the property tester.
	, test_runner_options     :: !CLITestRunnerOptions //* Options for the test runner.
	}

:: CLIPropertyTesterOptions =
	{ compile_only  :: !Bool //* Should the property tests only be compiled, not executed?
	, no_regenerate :: !Bool //* Should the property tests only be executed, not regenerated?
	, nr_of_tests   :: !?Int //* The number of tests to perform for each property.
	, only_modules  :: !?[String] //* Only test these module names (or search for property tests in all source directories).
	}

:: CLITestRunnerOptions =
	{ parallelization :: !?CLITestRunnerParallelization
	}

/**
 * Used for parallelizing tests over multiple test runner processes. Using this
 * option, the test-runner performs only the tests for which
 * `test index % total = index`.
 */
:: CLITestRunnerParallelization =
	{ index :: !Int //* The index of the current test process.
	, total :: !Int //* The total number of test processes being executed in parallel.
	}

/**
 * The test CLI command.
 *
 * @param Global options.
 * @param Test options.
 * @param Package.
 * @param Temporary directory.
 */
test :: !GlobalOptions !CLITestOptions !Package !FilePath !*World -> (!Bool, !*World)
