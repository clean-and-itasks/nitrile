implementation module Nitrile.CLI.Env

/**
 * Copyright 2021-2023 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import Data.Error
import Data.Func
import Data.Functor
import Data.Maybe
import Data.Tuple
import StdEnv
import System.OS
import Text

import Data.SemVer
import Nitrile.Build
import Nitrile.CLI
import Nitrile.CLI.Util
import Nitrile.CLI.Util.DependencySources
import Nitrile.Constants
import Nitrile.Package
import Nitrile.Target

env :: !GlobalOptions !Package !*World -> (!Bool, !*World)
env global_options=:{clm_linker,target,parallel_jobs} pkg w
	# (vars,w) = environmentVariables target w

	# pkg = markAllDependenciesAsNonOptional pkg
	# (mbSources,w) = getDependencySources False global_options (?Just pkg) ?None w
	  dependency_sources = fromOk mbSources
	| isError mbSources = fail (fromError mbSources) w
	# (mbClmVersion,w) = getClmVersion dependency_sources w
	# paths = srcInScope TestScope pkg ++ dependencyIncludePaths dependency_sources
	  mbClmflags = clmOptions mbClmVersion paths
	  clmflags = fromOk mbClmflags
	| isError mbClmflags = fail (fromError mbClmflags) w
	//CLMFLAGS are only emitted when clm is installed, i.e. when clmflags is non-empty
	# extra = if (clmflags =: []) [] [("CLMFLAGS", join " " clmflags)]
	= succeed ?None (seqSt (uncurry logVar) (vars ++ extra) w)
where
	clmOptions :: !(?Version) ![String] -> MaybeError String [String]
	clmOptions clm_version paths
		| isNone clm_version
			= Ok []
		# mbClmOptions = toClmOptions
			(fromJust clm_version)
			clm_linker
			parallel_jobs
			paths
			(fromMaybe (defaultClmBuildOptions ?None) pkg.Package.clm_options)
		  (config_options, path_options, cg_options, app_options) = fromOk mbClmOptions
		| isError mbClmOptions = liftError mbClmOptions
		| otherwise = Ok (config_options ++ path_options ++ cg_options ++ app_options)

logVar :: !String !String !*World -> *World
logVar var val w = log set w
where
	set = IF_WINDOWS
		(concat5 "$Env:" var " = '" val "'")
		(concat4 var "='" val "'")
