implementation module Nitrile.CLI.Update

/**
 * Copyright 2021-2022 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import Data.Error
import StdEnv
import StdMaybe
import System.Directory
import System.File
import System.FilePath
import Text
import Text.GenJSON

import Nitrile.CLI.Util
import Nitrile.Constants
import Nitrile.Registry
import Nitrile.Target

update :: !*World -> (!Bool, !*World)
update w
	# (mbConfigDir,w) = globalNitrileDir w
	| isError mbConfigDir = fail ("Failed to get local config directory: " +++ fromError mbConfigDir) w
	# configDir = fromOk mbConfigDir
	# (mbError,w) = ensureDirectoryExists configDir w
	| isError mbError = fail ("Failed to ensure local config directory exists: " +++ snd (fromError mbError)) w
	# (mbOutput,w) = callCurlAndCollectOutput
		[ "-L" // follow redirects
		, "-s", "-S" // silent, but output errors
		, "-f" // fail on server error
		, "--retry", "5"
		, "-m", "10" // timeout 10s
		, "-G" // use GET
		, "-d", "with_dependencies=true"
		, concat3 "https://" REGISTRY_HOST "/api/packages"
		]
		?None w
	| isError mbOutput = fail ("Failed to download the registry: " +++ fromError mbOutput) w
	# stdout = fromOk mbOutput
	# (mbError,w) = writeFile (configDir </> REGISTRY_COPY_FILE) stdout  w
	| isError mbError = fail ("Failed to write local registry copy: " +++ toString (fromError mbError)) w
	# w = log (concat3 "Wrote registry copy to " (configDir </> REGISTRY_COPY_FILE) ".") w
	# mbRegistryCopy = fromJSON (fromString stdout)
	| isNone mbRegistryCopy = fail "Failed to parse copy of registry." w
	| otherwise = writeConstraintSets allTargets configDir (fromJust mbRegistryCopy) w
where
	writeConstraintSets [] _ _ w = succeed ?None w
	writeConstraintSets [target:targets] configDir registryCopy w
		# (mbError,w) = writeFile file (toString (toJSON constraintSet)) w
		| isError mbError = fail ("Failed to write constraint set: " +++ toString (fromError mbError)) w
		# w = log (concat3 "Wrote constraint set to " file ".") w
		= writeConstraintSets targets configDir registryCopy w
	where
		constraintSet = toConstraintSet target registryCopy
		file = configDir </> constraintSetFile target
