definition module Nitrile.CLI.Global

/**
 * Copyright 2021-2022 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

from Data.SemVer import :: Version
from Nitrile.Target import :: Target

:: CLIGlobalCommand
	= Install !String !(?Version)
		//* Install the package with the given version (or, by default, the
		//* latest version).
	| List
		//* Display the globally installed packages.
	| Remove !String !Version
		//* Remove the package with the specified version.

/**
 * Implementation for the `nitrile global` CLI command.
 *
 * @param The selected target.
 * @param The subcommand of `nitrile global`.
 * @result True if success, False on failure.
 */
global :: !Target !CLIGlobalCommand !*World -> (!Bool, !*World)
