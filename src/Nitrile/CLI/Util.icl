implementation module Nitrile.CLI.Util

/**
 * Copyright 2021-2023 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

from Control.Applicative import instance pure ?, instance <*> ?
from Control.Monad import class Monad(bind), >>=, instance Monad ?
import Data.Either
import Data.Error
import Data.Func
import Data.Functor
import Data.List
import Data.Maybe
import Data.Tuple
import StdEnv
import System.Directory
import System.Environment
import System.File
import System.FilePath
import System.OS
import System.Process
import System.SysCall
import Text
import Text.GenJSON
import Text.YAML

import Data.SemVer
import Nitrile.CLI.Settings
import Nitrile.Constants
import Nitrile.Package
import Nitrile.Registry
import Nitrile.Target

instance toString ReadPackageError
where
	toString (MissingNitrileYml s) = s
	toString (OtherPackageError s) = s

log :: !String !*World -> *World
log s w
	# (io,w) = stdio w
	# io = io <<< s <<< "\n"
	# (_,io) = fflush io
	# (_,w) = fclose io w
	= w

succeed :: !(?String) !*World -> (!Bool, !*World)
succeed ?None w = (True, w)
succeed (?Just e) w = (True, log e w)

fail :: !String !*World -> (!Bool, !*World)
fail e w = (False, log e w)

checkSpecificTarget :: !Target -> MaybeError String ()
checkSpecificTarget t
	| t.platform=:AnyPlatform
		= Error "Cannot use --platform=any with this subcommand."
	| t.architecture.family=:AnyArchitectureFamily || t.architecture.bitwidth=:AnyBitwidth
		= Error "Cannot use a non-specific --arch with this subcommand."
		= Ok ()

logWarningForUnspecificTarget :: !String !Target !*World -> *World
logWarningForUnspecificTarget cmd target w
	| isError (checkSpecificTarget target)
		= log (concat3 "Warning: running '" cmd "' without a specific --platform and/or --arch.") w
		= w

packageSlur :: !Package !Target -> ?String
packageSlur pkg target = case (pkg.Package.name, pkg.Package.version) of
	(?Just name, ?Just version) -> ?Just $ join "-" [name, toString version, toString target]
	_ -> ?None

packageFile :: !Package !Target -> ?String
packageFile pkg target = flip (+++) ".tar.gz" <$> packageSlur pkg target

isPackageFile :: !String -> Bool
isPackageFile s
	| not (endsWith ".tar.gz" s) = False
	| otherwise = case reverse (split "-" (s % (0, size s-8))) of
		[arch, platform, version, name:_] ->
			isOk (parseArchitecture arch) &&
			isOk (parsePlatform platform) &&
			isOk (parseVersion version)
		_ ->
			False

environmentVariables :: !Target !*World -> (![(String, String)], !*World)
environmentVariables target w
	# (mbDir,w) = getCurrentDirectory w
	| isError mbDir = abort "Failed to get working directory\n"
	# curDir = fromOk mbDir
	  cleanHome = curDir </> homeDir target
	  cleanBin = cleanHome </> "bin"
	# (mbPath,w) = getEnvironmentVariable "PATH" w
	  (mbCi,w) = getEnvironmentVariable "GITLAB_CI" w
	  (mbProgramFiles,w) = getEnvironmentVariable "ProgramFiles" w
	# vars =
		[ ("CLEAN_HOME", cleanHome)
		, ("CLEANLIB", cleanHome </> "exe")
		, ("PATH", maybeAddPwshPath mbCi mbProgramFiles $ maybe cleanBin (prependPath cleanBin) mbPath)
		]
	= (vars, w)
where
	prependPath add path
		| add == path || startsWith add` path
			= path
			= add` +++ path
	where
		add` = add +++ IF_WINDOWS ";" ":"

	// On Windows in CI, chocolatey fails to update the path after pwsh is
	// installed with the installation script from clean-lang.org (for details,
	// see https://stackoverflow.com/a/46760714). This means we have to add the
	// path to PowerShell ourselves.
	maybeAddPwshPath (?Just ci) (?Just programFiles)
		| IF_WINDOWS False True
			= id
			= prependPath (programFiles </> "PowerShell" </> "7")
	maybeAddPwshPath _ _
		= id

setupEnvironmentVariables :: !Target !*World -> *World
setupEnvironmentVariables target w
	# (vars,w) = environmentVariables target w
	= seqSt (uncurry setEnvironmentVariable) vars w

getGitRefMatching :: !(?Version) !*World -> (!?String, !*World)
getGitRefMatching mbVersion w
	# (mbResult,w) = callProcessAndCollectOutput "git" ["tag", "-l", "--points-at", "HEAD"] ?None w
	  (exitCode,stdOut,_) = fromOk mbResult
	| isError mbResult || exitCode <> 0
		// If git is not installed, try to use GitLab predefined variables
		# (mbTag,w) = getEnvironmentVariable "CI_COMMIT_TAG" w
		| isJust mbTag && matches mbVersion (fromJust mbTag) = (mbTag, w)
		# (mbHash,w) = getEnvironmentVariable "CI_COMMIT_SHA" w
		| isJust mbHash && size (fromJust mbHash) > 0 = (mbHash, w)
		| otherwise = (?None, w)
	# tags =
		[ tag
		\\ line <- split "\n" stdOut, let tag = trim line
		| matches mbVersion tag
		]
	| not (isEmpty tags) = (?Just (hd tags), w)
	# (mbResult,w) = callProcessAndCollectOutput "git" ["show-ref", "--hash", "HEAD"] ?None w
	  (exitCode,stdOut,_) = fromOk mbResult
	| isError mbResult || exitCode <> 0 = (?None, w)
	# ref = trim stdOut
	| otherwise = (if (size ref == 0) ?None (?Just ref), w)
where
	matches (?Just version) tag
		| parseVersion tag == Ok version
			= True
		| size tag > 0 && tag.[0] == 'v' && parseVersion (tag % (1, size tag-1)) == Ok version
			= True
			= False
	matches ?None tag
		= False

getRegistry :: !*World -> (!MaybeError String Registry, !*World)
getRegistry w
	# (mbDir,w) = globalNitrileDir w
	| isError mbDir = (liftError mbDir, w)
	# (mbRegistry,w) = readFile (fromOk mbDir </> REGISTRY_COPY_FILE) w
	| isError mbRegistry = (Error (concat3 "Failed to read registry copy: " (toString (fromError mbRegistry)) " (did you run nitrile update?)."), w)
	# mbRegistry = fromJSON (fromString (fromOk mbRegistry))
	= (mb2error "Failed to parse registry copy." mbRegistry, w)

getConstraintSet :: !Target !*World -> (!MaybeError String ConstraintSet, !*World)
getConstraintSet target w
	# (mbDir,w) = globalNitrileDir w
	| isError mbDir = (liftError mbDir, w)
	# (mbConstraintSet,w) = readFile (fromOk mbDir </> constraintSetFile target) w
	| isError mbConstraintSet = (Error (concat3 "Failed to read constraint set: " (toString (fromError mbConstraintSet)) " (did you run nitrile update?)."), w)
	# mbConstraintSet = fromJSON (fromString (fromOk mbConstraintSet))
	= (mb2error "Failed to parse constraint set." mbConstraintSet, w)

readPackage :: !(?String) ![(String, String)] !FilePath !*World -> (!MaybeError ReadPackageError Package, !*World)
readPackage name nickelDefines dir w
	# (mbNickelPackage,w) = readFile (dir </> PACKAGE_FILE_NICKEL) w
	# foundNickel = isOk mbNickelPackage
	  nickelContent = fromOk mbNickelPackage
	| not foundNickel && not (isEmpty nickelDefines)
		= (Error (OtherPackageError "--define used but no nitrile.ncl file found."), w)
	# (mbPackage,w) = if foundNickel
		(packageForNickel nickelContent w)
		(appFst (mapError toString) $ readFile (dir </> PACKAGE_FILE) w)
	  package = fromOk mbPackage
	| isError mbPackage
		= (Error (MissingNitrileYml $ concat4 "Failed to read " (dir </> if foundNickel PACKAGE_FILE_NICKEL PACKAGE_FILE) ": " (fromError mbPackage)), w)
	# mbPackages = if foundNickel
		(loadMultipleYAML coreSchema package <|> loadYAML coreSchema package)
		(loadMultipleYAML coreSchema package)
	  packages = fst (fromOk mbPackages)
	| isError mbPackages
		= (Error (OtherPackageError $ concat4 "Failed to parse " PACKAGE_FILE ": " (printYAMLError (fromError mbPackages))), w)
	| isEmpty packages
		= (Error (OtherPackageError $ dir </> PACKAGE_FILE +++ " contains no documents."), w)
	| otherwise
		= case name of
			?None -> (Ok (hd packages), w)
			?Just name -> case find (\pkg -> pkg.Package.name == ?Just name) packages of
				?None -> (Error $ OtherPackageError (concat5 "Specified package " name " is not defined in " (dir </> PACKAGE_FILE) "."), w)
				?Just pkg -> (Ok pkg, w)
where
	// there is no instance Alternative (MaybeError e)
	(<|>) (Error _) r = r
	(<|>) l _ = l

	packageForNickel nickelContent w
		# (mbGlobalSettings,w) = getSettings w
		  nickelExe = fromMaybe ["nickel"] ((fromOk mbGlobalSettings).nickel >>= \{executable} -> executable)
		  [exe:args] = nickelExe
		| isError mbGlobalSettings = (liftError mbGlobalSettings, w)
		| isEmpty nickelExe = (Error "nickel executable was provided as an empty setting in the global settings.", w)
		# (mbErr,w) = runProcessIO exe (args ++
			[ "export", "--format", "yaml", "--"
			: concatMap (\(var,val) -> [concat4 "nitrile.options." var "=" val]) nickelDefines
			]) ?None w
		  (pHandle,pio=:{stdIn,stdOut,stdErr}) = fromOk mbErr
		| isError mbErr = (liftError (mapError snd mbErr), w)
		# (mbErr,w) =  writePipe nickelContent stdIn w
		| isError mbErr
			# (_,w) = closeProcessIO pio w
			= (liftError (mapError snd mbErr), w)
		# (mbErr,w) = closePipe stdIn w
		| isError mbErr
			# (_,w) = closeProcessIO pio w
			= (liftError (mapError snd mbErr), w)
		# (mbErr,w) = waitForProcess pHandle w
		| isError mbErr
			# (_,w) = closeProcessIO pio w
			= (liftError (mapError snd mbErr), w)
		# (mbErr,w) = readPipeBlockingMulti [stdOut,stdErr] w
		# (_,w) = closeProcessIO pio w
		= case mbErr of
			Ok [stdout,stderr]
				| stderr <> "" -> (Error stderr, w)
				| otherwise -> (Ok stdout, w)
			Error e -> (Error ("could not read nickel output: " +++ snd e), w)

printYAMLError :: !(Either YAMLError YAMLErrorWithLocations) -> String
printYAMLError (Left error) = toString error
printYAMLError (Right {error,locations}) = join "\n" $
	[ toString error
	: case printLocations locations of
		[] -> []
		stack
			# end = hd stack
			# init = reverse (tl stack)
			-> ["Parse stack:":map (flip (+++) ", in") init ++ [end]]
	]
where
	printLocations [ADT _:rest] = printLocations rest
	printLocations [Constructor c:rest] = ["\tconstructor " +++ c:printLocations rest]
	printLocations [Record _:rest] = printLocations rest
	printLocations [Field f:rest] = ["\tfield " +++ f:printLocations rest]
	printLocations [SequenceIndex i:rest] = ["\tsequence index " +++ toString i:printLocations rest]
	printLocations [] = []

callCurlAndPassIO :: ![String] !(?String) !*World -> (!MaybeError String (), !*World)
callCurlAndPassIO args dir w
	# (mbExitCode,w) = callProcessAndPassIO "curl" args dir w
	| isError mbExitCode = (failedToRunCurlError mbExitCode,w)
	| fromOk mbExitCode <> 0
		= (Error $ curlErrorString $ fromOk mbExitCode,w)
		= (Ok (),w)

callCurlAndCollectOutput :: ![String] !(?String) !*World -> (!MaybeError String String, !*World)
callCurlAndCollectOutput args dir w
	# (mbOutput,w) = callProcessAndCollectOutput "curl" args dir w
	| isError mbOutput = (failedToRunCurlError mbOutput, w)
	# (exitCode,stdout,stderr) = fromOk mbOutput
	| exitCode <> 0
		= (Error $ concat3 (curlErrorString exitCode) ": " stderr, w)
		= (Ok stdout, w)

failedToRunCurlError :: !(MaybeError OSError a) -> MaybeError String b
failedToRunCurlError mbError = Error ("failed to run curl: " +++ snd (fromError mbError))

curlErrorString :: !Int -> String
curlErrorString exitCode = "curl exited with error code " +++ toString exitCode
