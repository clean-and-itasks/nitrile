implementation module Nitrile.Package.Migrate

/**
 * Copyright 2022 the authors (see README.md).
 *
 * This file is part of Nitrile.
 *
 * Nitrile is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Nitrile is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with Nitrile. If not, see <https://www.gnu.org/licenses/>.
 *
 * The software is licensed under additional terms under section 7 of the GNU
 * Affero General Public License; see the LICENSE file for details.
 */

import Control.Monad
import Data.Bifunctor
import Data.Error
import Data.Func
import Data.Functor
import Data.List
import Data.Maybe
import Data.Tuple
import StdDebug
import StdEnv
from Text import concat3, concat5
import Text.YAML
import Text.YAML.Compose
import Text.YAML.Construct
import Text.YAML.Parse

import Data.SemVer
import Nitrile.Constants
import Nitrile.Package

migrate :: !YAMLSchema !YAMLNode -> MaybeError YAMLErrorWithLocations YAMLNode
migrate schema node=:{content=YAMLMapping kvs} = case mbFormatVersion of
	?Just {value} ->
		first (pushErrorLocation (Field "format_version")) (constructFromYAML schema value) >>= \version ->
		first
			(withoutErrorLocations o InvalidContent)
			(up version {node & content=YAMLMapping rest})
	?None ->
		first
			(withoutErrorLocations o InvalidContent)
			(up {major=0,minor=4,patch=3} (trace_n "no format_version set; assuming 0.4.4" node))
where
	(mbFormatVersion,rest) = appFst listToMaybe $ partition (\{key} -> key=:{content=YAMLScalar _ "format_version"}) kvs
migrate _ _ = Error (withoutErrorLocations (InvalidContent "expected a mapping"))

//* Updates the structure to the current format version
up :: !Version -> YAMLNode -> MaybeError String YAMLNode
up v | v == currentVersion =
	pure

up v=:{major=0,minor=4,patch} | 22 <= patch && patch <= 25 =
	up {v & patch=26}

up v=:{major=0,minor=4,patch=21} =
	changeCheckIndicesDefault >=>
	up {v & patch=22}
where
	changeCheckIndicesDefault =
		modifyKey "clm_options" alterValue >=>
		modifyKey "build" modBuild >=>
		modifyKey "tests" modTests >=>
		modifyKey "rules" (everyValue $ const $ modifyKey "extra_build" modBuild) >=>
		modifyKey "rules" (everyValue $ const $ modifyKey "extra_tests" modTests)

	modBuild = everyValue $ const $ modifyKey "script" $ everyElem $ ifIsMapping (modifyKey "clm" alterValue) pure
	modTests = everyValue $ const $
			modifyKey "compilation" (modifyKey "clm_options" alterValue) >=>
			modifyKey "properties" (modifyKey "clm_options" alterValue)

	alterValue = alterKey "check_indices" (Ok o ?Just o fromMaybe (yaml "false"))

up v=:{major=0,minor=4,patch} | 13 <= patch && patch <= 20 =
	up {v & patch=21}

up v=:{major=0,minor=4,patch=12} =
	checkShorthandSyntaxDependencies >=>
	checkExceptAndOnlyModules >=>
	up {v & patch=13}
where
	checkShorthandSyntaxDependencies =
		modifyKey "dependencies" (everyValue check) >=>
		modifyKey "rules" (everyValue $ const $ modifyKey "extra_dependencies" $ everyValue check)
	where
		check key value = ifIsScalar
			(const (pure value))
			(const (Error "the shorthand syntax for dependencies requires format_version >= 0.4.13"))
			key

	checkExceptAndOnlyModules =
		modifyKey "tests" checkTests >=>
		modifyKey "rules" (everyValue $ const $ modifyKey "extra_tests" checkTests)
	where
		checkTests = everyValue $ const $ modifyKey "compilation" (check "except_modules" >=> check "only_modules")

		check key = ifHasKey key
			(const (Error ("tests:compilation:" +++ key +++ " requires format_version >= 0.4.13")))
			pure

up v=:{major=0,minor=4,patch=11} =
	up {v & patch=12}

up v=:{major=0,minor=4,patch=10} =
	checkRequiredDependencies >=>
	up {v & patch=11}
where
	checkRequiredDependencies =
		modifyKey "build" checkBuild >=>
		modifyKey "tests" checkTests >=>
		modifyKey "rules" (everyValue $ const $ modifyKey "extra_build" checkTests) >=>
		modifyKey "rules" (everyValue $ const $ modifyKey "extra_tests" checkTests)

	checkBuild = everyValue $ const $ check "build"
	checkTests = everyValue $ const $ check "tests"

	check parent = ifHasKey "required_dependencies"
		(const (Error (parent +++ ":required_dependencies requires format_version >= 0.4.11")))
		pure

up v=:{major=0,minor=4,patch=9} =
	checkUndecidableInstances >=>
	up {v & patch=10}
where
	checkUndecidableInstances =
		modifyKey "clm_options" check >=>
		modifyKey "build" (everyValue $ const $ modifyKey "script" $ everyElem $ ifIsMapping (modifyKey "clm" check) pure) >=>
		modifyKey "tests" (everyValue $ const $
			modifyKey "compilation" (modifyKey "clm_options" check) >=>
			modifyKey "properties" (modifyKey "clm_options" check))

	check = ifHasKey "undecidable_instances"
		(const (Error "clm_options:undecidable_instances requires format_version >= 0.4.10"))
		pure

up v=:{major=0,minor=4,patch} | 3 <= patch && patch <= 8 =
	up {v & patch=9}

up v
	| v > currentVersion = const $ Error $ concat5
		"this version of nitrile cannot parse nitrile.yml with format_version: " (toString v)
		" (max is " (toString CURRENT_VERSION) "); download a newer version"
	| otherwise = const $ Error $ concat3
		"migration from format version " (toString v) " is not implemented"

//* Shorthand for hardcoded YAML values.
yaml :: !String -> YAMLNode
yaml s = hd $ fromOk $ composeYAMLGraphs $ fromOk $ parseYAMLStream s

//* Modify all elements in a sequence.
everyElem :: !(YAMLNode -> MaybeError String YAMLNode) !YAMLNode -> MaybeError String YAMLNode
everyElem f n=:{content=YAMLSequence elems} =
	mapM f elems <$&> \elems ->
	{n & content=YAMLSequence elems}
everyElem _ _ =
	Error "nitrile.yml migration expected a sequence"

//* Modify all values in a mapping.
everyValue :: !(YAMLNode YAMLNode -> MaybeError String YAMLNode) !YAMLNode -> MaybeError String YAMLNode
everyValue f n=:{content=YAMLMapping kvs} =
	mapM (\kv=:{key,value} -> f key value <$&> \v -> {kv & value=v}) kvs <$&> \kvs ->
	{n & content=YAMLMapping kvs}
everyValue _ _ =
	Error "nitrile.yml migration expected a mapping"

//* Modify the value of a key in a mapping, if it exists.
modifyKey :: !String !(YAMLNode -> MaybeError String YAMLNode) !YAMLNode -> MaybeError String YAMLNode
modifyKey key f n = alterKey key f` n
where
	f` n = case n of
		?None -> Ok ?None
		?Just v -> ?Just <$> f v

//* Alter the (optional) value of a key in a mapping, or delete it.
alterKey :: !String !((?YAMLNode) -> MaybeError String (?YAMLNode)) !YAMLNode -> MaybeError String YAMLNode
alterKey searchKey f n=:{content=YAMLMapping kvs} =
	let oldMbV = listToMaybe [value \\ {key,value} <- kvs | key matchesString searchKey] in
	f oldMbV <$&> \newMbV ->
		{ n
		& content = YAMLMapping case newMbV of
			?Just v -> [{key=yaml searchKey, value=v} : filter (\{key} -> not (key matchesString searchKey)) kvs]
			?None -> [kv \\ kv <- kvs | not (kv.key matchesString searchKey)]
		}
alterKey key _ _ =
	Error (concat3 "could not modify key '" key "' while migrating nitrile.yml; value is not a mapping")

//* Do different things depending on whether a node is a scalar.
ifIsScalar :: !(YAMLNode -> MaybeError String YAMLNode) !(YAMLNode -> MaybeError String YAMLNode) !YAMLNode -> MaybeError String YAMLNode
ifIsScalar ifTrue ifFalse n=:{content} = if (content=:(YAMLScalar _ _)) (ifTrue n) (ifFalse n)

//* Do different things depending on whether a node is a mapping.
ifIsMapping :: !(YAMLNode -> MaybeError String YAMLNode) !(YAMLNode -> MaybeError String YAMLNode) !YAMLNode -> MaybeError String YAMLNode
ifIsMapping ifTrue ifFalse n=:{content} = if (content=:(YAMLMapping _)) (ifTrue n) (ifFalse n)

//* Do different things depending on whether a key exists in a mapping.
ifHasKey :: !String !(YAMLNode -> MaybeError String YAMLNode) !(YAMLNode -> MaybeError String YAMLNode) !YAMLNode -> MaybeError String YAMLNode
ifHasKey searchKey ifTrue ifFalse n=:{content=YAMLMapping kvs} =
	if (any (\{key} -> key matchesString searchKey) kvs) (ifTrue n) (ifFalse n)
ifHasKey key _ _ _ =
	Error (concat3 "could not check for existence of key '" key "' while migrating nitrile.yml; value is not a mapping")

(matchesString) infix :: !YAMLNode !String -> Bool
(matchesString) key searchKey
	# key = constructFromYAML coreSchema key
	= isOk key && fromOk key == searchKey
