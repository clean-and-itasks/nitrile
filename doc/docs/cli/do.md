---
vim: noexpandtab tabstop=2 shiftwidth=2
---

# `nitrile do`

This command runs [`actions`](../package-configuration/reference.md#actions)
defined in the package configuration.

## Usage

There are currently two ways to use `nitrile do`:

- `nitrile do ACTION [-- ARG1 ARG2 ...]` runs the `ACTION` defined in the
	package configuration. Any arguments after `--` are not parsed by Nitrile but
	passed to the script as `$@` (Shell) or `$args` (PowerShell).
- `nitrile do --list` does not run any action but lists the actions defined in
  the configuration file.
