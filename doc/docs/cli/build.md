---
vim: noexpandtab tabstop=2 shiftwidth=2
---

# `nitrile build`

This command builds the package according to the
[`build`](../package-configuration/reference.md#build) settings in
the package configuration.

## Options

### `--list`

Only print build goal names. The builds are not run.

### `--only=GOAL1[,GOAL2,..]`

Only build goals `GOAL1`, `GOAL2`, etc. (and their dependencies).
