---
vim: noexpandtab tabstop=2 shiftwidth=2
---

# Global settings

For some [global options](global-options.md), a default value can be set in a
global settings file. This file is `nitrile-settings.yml` and should be located
in the global Nitrile directory.

## `clm`

This field can be used to control the behaviour of
[`clm`](../package-configuration/reference.md#buildscriptclm) jobs. The
following fields are accepted:

- `linker`: specify the linker to use (see
  [`--clm-linker`](global-options.md#-clm-linkerlinker))
- `parallel_jobs`: specify how many subprocesses can be used in parallel (see
  [`--parallel-jobs`](global-options.md#-parallel-jobsn))

For example:

```yml
clm:
  linker: lld
  parallel_jobs: 4
```

## `nickel`

This field can be used to control the
[Nickel](../package-configuration/nickel.md) executable that is used if a
`nitrile.ncl` file is provided for the project. Currently, only the
`executable` field is accepted. The default setting for the executable is
`['nickel']`. An empty list may not be provided for this field, and `export`
will be added to the arguments.

For example:

```yml
nickel:
  executable: ['docker', 'run', '--rm', '-i', 'ghcr.io/tweag/nickel:1.8.0']
```

will use `docker run --rm -i ghcr.io/tweag/nickel:1.8.0 export --format=yaml`
and provide the content of `nitrile.ncl` on stdin.
