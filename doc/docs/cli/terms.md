---
vim: noexpandtab tabstop=2 shiftwidth=2
---

# `nitrile terms`

This command prints the full legal terms of the program.
