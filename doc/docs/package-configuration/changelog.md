---
vim: noexpandtab tabstop=2 shiftwidth=2
---

# Changes to the package configuration

Since Nitrile 0.4.4, the package configuration has an optional
[`format_version`](reference.md#format_version) field that specifies the
version of Nitrile for which the configuration file is written. Newer versions
of Nitrile can work with configuration files written for older versions by
automatically migrating the configuration. This page lists the changes to the
configuration file format for each Nitrile version. It should be checked when
upgrading `format_version`, as upgrading this field may change behavior.

Not every Nitrile version introduces changes to the package configuration.
Backwards incompatible changes are marked in orange.

### 0.4.22

!!! warning ""
	- The default of
		[`clm_options:check_indices`](reference.md#clm_options) is now `true`.

### 0.4.13

!!! info ""
	- Added shorthand syntax for [`dependencies`](reference.md#dependencies).
	- Added
		[`tests:compilation:except_modules`](reference.md#testscompilationexcept_modules).
	- Added
		[`tests:compilation:only_modules`](reference.md#testscompilationonly_modules).

### 0.4.11

!!! info ""
	- Added
		[`build:required_dependencies`](reference.md#buildrequired_dependencies)
		and
		[`tests:required_dependencies`](reference.md#testsrequired_dependencies).

### 0.4.10

!!! info ""
	- Added [`clm_options:undecidable_instances`](reference.md#clm_options).

### 0.4.4

!!! info ""
	- First version with [`format_version`](reference.md#format_version).
